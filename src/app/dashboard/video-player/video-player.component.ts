import { Component } from '@angular/core';
import { Observable, EMPTY } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { Video } from '../types';
import { AppState } from '../../types';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.less']
})
export class VideoPlayerComponent {
  video: Observable<Video>;

  constructor(ar: ActivatedRoute, store: Store<AppState>) {
    this.video = ar.queryParamMap.pipe(
      map(qpm => qpm.get('id')),
      switchMap(id => !id ? EMPTY : store.select(state =>
        state.dashboard.videos.find(v => v.id === id) as Video)),
    );
  }
}
