import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatNativeDateModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { StoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { VideoDashboardComponent } from './video-dashboard/video-dashboard.component';
import { VideoListComponent } from './video-list/video-list.component';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { StatFiltersComponent } from './stat-filters/stat-filters.component';
import { VideoListItemComponent } from './video-list-item/video-list-item.component';
import { DashboardState } from './types';
import { DASHBOARD_REDUCERS, loadVideos } from './state';
import { DashboardEffectsService } from './dashboard-effects.service';
import { AppState } from '../types';

const routes: Routes = [
  { path: '', component: VideoDashboardComponent },
];

@NgModule({
  declarations: [
    VideoDashboardComponent,
    VideoListComponent,
    VideoPlayerComponent,
    StatFiltersComponent,
    VideoListItemComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatListModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatCheckboxModule,
    StoreModule.forFeature<DashboardState>('dashboard', DASHBOARD_REDUCERS),
    EffectsModule.forFeature([DashboardEffectsService]),
  ]
})
export class DashboardModule {
  constructor(store: Store<AppState>) {
    store.dispatch(loadVideos()); // ROOT_EFFECTS_INIT is too early since feature is lazy loaded
  }
}
