export interface ViewDetails {
  age: number;
  region: string;
  date: string;
}

export interface UnratedVideo {
  title: string;
  author: string;
  id: string;
  viewDetails: ViewDetails[];
}

export interface Video extends UnratedVideo {
  rating: number;
}

export interface DashboardState {
  videos: Video[];
}
