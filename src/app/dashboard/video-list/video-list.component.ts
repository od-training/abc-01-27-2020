import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { Video } from '../types';
import { AppState } from '../../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.less']
})
export class VideoListComponent {
  allVideos: Observable<Video[]>;

  constructor(store: Store<AppState>) {
    this.allVideos = store.pipe(
      select(state => state.dashboard.videos),
    );
  }
}
