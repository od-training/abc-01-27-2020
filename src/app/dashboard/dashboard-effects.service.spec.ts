import { TestBed } from '@angular/core/testing';

import { DashboardEffectsService } from './dashboard-effects.service';

describe('DashboardEffectsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashboardEffectsService = TestBed.get(DashboardEffectsService);
    expect(service).toBeTruthy();
  });
});
