import { DashboardState } from './dashboard/types';

export interface AppState {
  dashboard: DashboardState;
}
