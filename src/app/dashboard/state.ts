import { createAction, createReducer, on, props, ActionReducerMap, Action } from '@ngrx/store';
import { InjectionToken } from '@angular/core';

import { Video, DashboardState } from './types';

export const receivedVideos = createAction(
  'RECEIVED_VIDEOS',
  props<{ videos: Video[] }>()
);

export const loadVideos = createAction('LOAD_VIDEOS');

export const videosReducer = createReducer(
  [] as Video[],
  on(receivedVideos, (_state, action) => action.videos),
);

// Register your reducers with NgRx framework in an AOT-compatible way
export const DASHBOARD_REDUCERS = new InjectionToken<
  ActionReducerMap<DashboardState, Action>
>('Root reducers token', {
  factory: () => ({
    videos: videosReducer,
  })
});
