import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { Video, UnratedVideo } from './types';

const api = 'http://localhost:8085/videos';

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  constructor(private http: HttpClient) {}

  getVideos(): Observable<Video[]> {
    return this.http.get<UnratedVideo[]>(api).pipe(map(
      videos => videos.map(v => ({ ...v, rating: Math.ceil(Math.random() * 5) })),
    ));
  }

  getVideo(id: string): Observable<UnratedVideo> {
    return this.http.get<UnratedVideo>(`${api}/${id}`);
  }
}
