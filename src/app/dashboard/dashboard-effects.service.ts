import { Injectable } from '@angular/core';
import { switchMap, map } from 'rxjs/operators';
import { createEffect, Actions, ofType } from '@ngrx/effects';

import { VideoService } from './video.service';
import { receivedVideos, loadVideos } from './state';

@Injectable()
export class DashboardEffectsService {

  constructor(private actions: Actions, private vs: VideoService) {}

  loadVideos = createEffect(() => this.actions.pipe(
    ofType(loadVideos),
    switchMap(() => this.vs.getVideos()),
    map(videos => receivedVideos({ videos })),
  ));

}
