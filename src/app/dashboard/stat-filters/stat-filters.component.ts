import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.less']
})
export class StatFiltersComponent {

  group: FormGroup;

  constructor(fb: FormBuilder) {
    this.group = fb.group({
      region: ['all'],
      fromDate: ['2006-01-01'],
      toDate: ['2020-01-01'],
      age: fb.group({ // Can nest FormGroup & FormArray
        under18: [true],
        '18to40': [false],
        '40to60': [true],
        over60: [false],
      }),
    });
  }

}
